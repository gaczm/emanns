# *Luźne notatki*

## Struktura agentowa
Ważnym założeniem przyjętym przy realizacji projektu jest prostota użytkowania. Dlatego też istotne było wprowadzanie separacji między zachowaniem agenta, a elementami infrastrukturalnymi: 

- położeniem agenta w strukturze
- sposobem uruchamiania (wątek, proces na zdalnej maszynie, prosty krokowy działający wewnątrz wątku)
- sposobem komunikacji międzyagentowej (protokół komunikacyjny ma być tak samo obsługiwany bez względu na to jak agent jest uruchamiany przez platformę i tego gdzie w hierarchii się on znajduje)

Interfejs który użytkownik ma zaimplementować jest maksymalnie uproszczony i wygląda następująco:
```
AgentBehavior:
   + doStep(int step): void
```

Dodatkowo istnieje interfejs ``Agent``, który jest reprezentacją agenta "z zewnąrz": jesteśmy w nim świadomi rodzica, dzieci, możemy również wykonać na nich operacje. Interfejs ten używany jest np przy mechanizmie akcji.

### Pod maską agenta

W tym akapicie opisane zostanie w jaki sposób osięgnięta została wspomniana wcześniej separacja.

Na poniższym rysunku widzimy diagram klas przedstawiający strukturę agnetów:

![Diagram klas przedstawiający agenta](https://bytebucket.org/gaczm/emanns/raw/03ef13e6a9e0580fadbe45ad0850b143f435274a/docs/imgs/struktura.png?token=220bfeed80de9d78e84670818754622f662157a2)

Poza wspomnianymi wcześniej klasami ``Agent`` i ``AgentBehavior`` mamy jeszcze 3 inne: ``Workplace``, ``AgentWithBehavior`` i ``AgentWithBehaviorImpl``. Są one stricte wewnętrzne, a ich dokładne przeznaczanie wyjaśnione jest dokładniej na rysunku.

## DSL do konfiguracji
DSL tworzony jest w języku Groovy posiadającym bardzo dobre wsparcie dla tworzenia własnych języków domenowych.

Przykładowa konfiguracja:

```
configuration {
  workplace {
    agent {
      name = "Board Fragment 1"
      agentClass = my.domain.BoardFragment
      
      agent {
        agentClass = my.domain.Ant
      }
      agent {
        agentClass = my.domain.Ant
      }
    }
    agent {
      name = "Board Fragment 2"
      agentClass = my.domain.BoardFragment

      agent {
        agentClass = my.domain.Ant
      }
    }
  }
}
```

Co istotne, nadal jest to poprostu Groovy, więc w pliku konfiguracyjnym środowisko możemy korzystać z wszelkich udogodnień oferowanych przez język, np:


```
def numberOfFragments = 16
def antsPerFragment = 10

import my.domain.BoardFragment
import my.domain.Ant

configuration {
  workplace {
    (0..numberOfFragments).each { fragmentIndex -> 
      agent {
        name = "Board Fragment $fragmentIndex"
        agentClass = BoardFragment
        separateThread = true

        antsPerFragment.times { agent { agentClass = Ant }  }
      }
    }
  }
}
```

## Akcje
W implementacji platformy będziemy próbować nieco innego (w stosunku do tego co jest w jAge-u) podejścia do akcji. Akcje są zlecane do wykonania, a następnie wykonują się na wszystkich agentach w sąsiedztwie. To agent (poprzez odpowiednie, zdefiniowane w jego definicji predykaty) decyduje czy akcja może zostać wykonana czy też nie.










