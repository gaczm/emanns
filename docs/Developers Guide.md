#Evolutionary Multi-Agent No-name System
<center style="font-size: 18px">Wersja 0.1.0</center>

## Cele

W projekcie skupiamy się na dostarczeniu podstawowej funkcjonalności platformy agentowej: 

- prostego do opanowania i przejrzystego języka do konfiguracji
- mechanizmu akcji
- mechanizmu zapytań

Nie zajmujemy się natomiast problemem rozpraszania platformy na wiele maszyn fizycznych.


### Struktura agentowa
W związku z chęcią uczynienia środowiska jak najprostszym w użyciu istotna była prosta struktura agentowa. Dlatego też ważne było wprowadzanie separacji między zachowaniem agenta, a elementami infrastrukturalnymi: 

- położeniem agenta w strukturze
- sposobem uruchamiania (wątek, proces na zdalnej maszynie, prosty krokowy działający wewnątrz wątku)
- sposobem komunikacji międzyagentowej (protokół komunikacyjny ma być tak samo obsługiwany bez względu na to jak agent jest uruchamiany przez platformę i tego gdzie w hierarchii się on znajduje)

Interfejs który użytkownik ma zaimplementować jest maksymalnie uproszczony i wygląda następująco:
```
AgentBehavior:
   + doStep(int step): void
```

Dodatkowo istnieje interfejs ``Agent``, który jest reprezentacją agenta "z zewnąrz": jesteśmy w nim świadomi rodzica, dzieci, możemy również wykonać na nich operacje. Interfejs ten używany jest np przy mechanizmie akcji.

### Pod maską agenta

W tym akapicie opisane zostanie w jaki sposób osięgnięta została wspomniana wcześniej separacja.

Na poniższym rysunku widzimy diagram klas przedstawiający strukturę agnetów:

![Diagram klas przedstawiający agenta](https://bytebucket.org/gaczm/emanns/raw/55912dd87e189b31e60806b074810b373090299e/docs/imgs/struktura-updated.png?token=b1103ff30711301bc50bde2128328b45d4536358)

Poza wspomnianymi wcześniej klasami ``Agent`` i ``AgentBehavior`` mamy jeszcze 3 inne: ``Workplace``, ``InternalAgentRepresentation`` i ``InternalAgentRepresentationImpl``. Są one stricte wewnętrzne, a ich dokładne przeznaczanie wyjaśnione jest dokładniej na rysunku.

## Konfiguracja platformy

Do konfiguracji platformy służy DSL w języku Groovy.

Ogólna struktura pliku wygląda następująco:

```
configuration {
	// ustawienia platformy
	workplace {
		agent {
			// ustawienia agenta
            // akcje wykonywane przez agenta po każdym kroku
			// opcjonalnie kolejne zagnieżdżone agenty
		}
		// konfiguracja kolejnych agentów
	}
	// konfiguracja kolejnych workplace-ów
}
```

Dostępne ustawienia platformy:

<table>
<tr>
    <td>stopCondition</td>
    <td>Obiekt lub klasa typu <code>pl.edu.agh.ap.configuration.StopCondition</code>. Określa warunek stopu obliczeń</td>
</tr>
<tr>
    <td>computationDurationInSeconds</td>
    <td>Ustawia warunek stopu na zakończenie obliczeń po upływie określonego czasu</td>
</tr>
</table>


Dostępne ustawienia agenta:

<table>
<tr>
    <td>agentClass</td>
    <td>Klasa typu <code>pl.edu.agh.ap.agent.AgentBehavior</code>. Określa zachowanie agenta</td>
</tr>
<tr>
    <td>name</td>
    <td>Nazwa agenta</td>
</tr>
<tr>
    <td>settings</td>
    <td>Mapa (String -> Object) z dodatkowymi ustawieniami które mają być przekazane do agenta</td>
</tr>
</table>

Dodatkowo należy pamiętać że mamy dostęp do wszelkich elementów składniowych Grooviego: pętli, zmiennych, instrukcji warunkowych, itp.

Przykładowa konfiguracja:

```
final NUMBER_OF_FRAGMENTS = 16
final FRAGMENTS_IN_ROW = 4
final ANTS_PER_FRAGMENT = 5

final SETTINGS = [
  "numberOfFragments": NUMBER_OF_FRAGMENTS,
  "fragmentsInRow": FRAGMENTS_IN_ROW
]

configuration {
  workplace {
    
    actions = [my.domain.actions.BoardMigrationAction, my.domain.actions.AntExterminationAction]
  
    (1..NUMBER_OF_FRAGMENTS).each { fragmentIndex ->
      agent {
        name = "Board Fragment $fragmentIndex"
        agentClass = my.domain.BoardFragment
        settings = SETTINGS
        
        (1..ANTS_PER_FRAGMENT).each { antIndex ->
          agent {
            agentClass = my.domain.Ant
            name = "ant $fragmentIndex.antIndex"
          }
        }
      }
    }
  }
}
```

## Akcje 

Akcje w dużym uproszczeniu są czynnościami mającymi się wykonać na konkretnym zestawie agentów. Decyzja o wykonaniu akcji (zleceniu jej 
wykonania) nie jest dokonywane dynamicznie przez konkretnego agenta w trakie jego działania, a deklarowana w pliku konfiguracyjnym. 
Nie decydujemy jednak sami, który konkretnie agent zostanie objęty wykonaniem danej akcji - ustalamy jedynie poziom w hierarchi agentów, dla 
którego dany zestaw akcji zostanie wykonany po każdym kroku. 

Dla następującej konfiguracji: 

```
agent {
  name = "Parent agent"
  agentClass = test.ParentAgent
  actions = [test.action.TransferEnergyAction]

  (1..10).each { agentIndex ->
    agent {
      agentClass = test.ChildAgent
      name = "Child agent $agentIndex"
    }
  }
}
```

otrzymujemy hierarchię agentów, z jednym agentem rodzicem ("Parent agent") i dziesięcioma agentami potomkami ("Child agent"). Po każdym 
kroku agent rodzic wykonuje na każdym swoim dziecku prefefiniowany zestaw akcji, tutaj: akcję transferu energii.

Sam interfejs akcji wygląda następująco:
```
public interface Action {
    void execute(Agent<?> parent, Collection<Agent<?>> agents);
}
```

Interfejs posiada jedną metodą, która przyjmuje agenta zlecającego akcję oraz zestaw agentów, na których ma wykonać się akcja.
Sama akcja zaś odbywa się na agencie umiejscowionym już w strukturze agentów (dzięki czemu możliwa jest inegerencja w nią). 

## Zapytania

Zapytania zostały zrealizowane przy użyciu wprowadzonych w Javie 8 strumieni. Przykładowe zapytanie wygląda następująco:

```
agentWithGreatestNumber = query(AgentWithMagicNumber.class)
    .max((a1, a2) -> a1.getMagicNumber() - a2.getMagicNumber())
    .get()
```

## Przykładowe wykorzystanie platformy

### Zdefiniowanie logiki agenta

Pierwszym krokiem aby móc dokonać jakichkolwiek obliczeń niezbędne jest dostarczenie implementacji klasy ``AgentBehavior`` . Jedynym co 
musimy zrobić to nadpisać metodę ``doStep(int stepNumber)`` , w której dokonywane jest konkretne obliczenie. 

```
package trivialExample;

public class DummyAgent extends AgentBehavior {

    private int energy = new Random().nextInt(100_000);

    @Override
    public void doStep(int stepNumber) {
        heavyComputations();
    }

    private void heavyComputations() {
        try {
            TimeUnit.MILLISECONDS.sleep(50);
        } catch (InterruptedException ignored) {
            Thread.currentThread().interrupt();
        }
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }
}
```

Tak zaimplementowana klasa, oprócz samej logiki wykonywania obliczeń może posiadać również stan (tu reprezentowany przez pole ``energy``), który
może się zmieniać w trakcie pracy agenta. Jeżeli chcemy zaimplementować akcję bazującą na tym stanie (lub go modyfikującą) niezbędne jest dostarczenie
odpowiednich metod (``getEnergy()`` oraz ``setEnergy(int energy)``).

### Stworzenie konfiguracji

Kolejnym krokiem jest stworzenie pliku konfiguracyjnego. My chcemy zdefiniować sobie prostą hierarchię z jednym workplace'm , posiadającego
dwóch agentów potomnych. Konfiguracja będzie miała postać : 

```
configuration {
    
    workplace {

        repeat(2) { index ->
            agent {
                agentClass = trivialExample.DummyAgent
            }
        }
    }
    
}
```
Struktura została zdefiniowana poprawnie. Takie obliczenie trwałoby jednak w "nieskończoność" - nie zdefiniowaliśmy żadnego warunku
stopu. Ustalimy sobie czas trwania obliczeń na 10 sekund. W tym celu musimy umieścić na poziomie konfiguracji platformy następującą linijkę:
```
configuration {

    computationDurationInSeconds = 10

    workplace {
      ...
    }
```

### Uruchomienie

Posiadamy już logikę obliczenia (implementację klasy ``AgentBehavior``) oraz strukturę, na której ma zostać wykonane (zdefiniowaną w pliku
konfiguracyjnym) - teraz potrzebujemy tylko uruchomić naszą platformę. W tym celu należy:

1. Wczytać konfigurację za pomocą dostarczonej klasy ``ConfigurationLoader``
```
Configuration configuration = ConfigurationLoader.loadFromClassPath("trivialExample/computation.cfg");
```
2. Zainstancjonować platformę otrzymaną wcześniej konfiguracją
```
Platform platform = new Platform(configuration);
```
3. Uruchomić obliczenia
```
platform.run();
```
### Dodanie akcji

Żeby dodać akcję należy ją najpierw zdefiniować. Definicja akcji to nic innego jak zaimplementowanie interfejsu ``Action``. Stworzymy
prostą akcję reprodukcji. Akcja będzie polegać na tym, że w losowym momencie (na przykład raz na kilka tysięcy kroków) do agenta, który zlecił
wykonanie akcji dodany zostanie kolejny potomek, który otrzyma połowę energii rodzica. Implementacja będzie wyglądała następująco:
```
package trivialExample;

public class ReproductionAction implements Action {

    private Random random = new Random();

    @Override
    public void execute(Agent<?> parent, Collection<Agent<?>> agents) {

        Collection<Agent<DummyAgent>> castedAgents = (Collection) agents;

        for (Agent<DummyAgent> agent : castedAgents) {
            if (random.nextInt(100) == 0) {
            
                //stworzenie nowego agenta na podstawie innych potomków
                Agent<DummyAgent> newAgent = AgentBuilder.baseOn(agent).build();
                
                //ustalenie poziomu energii dla nowego agenta oraz dla rodzica
                int energyForNewAgent = agent.getBehavior().getEnergy() / 2;
                newAgent.getBehavior().setEnergy(energyForNewAgent);
                agent.getBehavior().setEnergy(agent.getBehavior().getEnergy() - energyForNewAgent);
                
                //dodanie agenta do struktury
                parent.addChild(newAgent);
            }
        }
    }

}
```

Żeby taka akcja miała miejsce niezbędne jest zadeklarowanie jej w pliku konfiguracyjnym. W ustalonej przez nas wyżej hierarchii agentem rodzicem
jest ``workplace`` i posiada on początkowo dwóch potomków klasy ``DummyAgent``. Chcemy aby rodzic wykonywał akcję reprodukcji. W tym celu dodajemy
następujący wpis w pliku konfiguracyjnym: 
```
workplace {
    ...
    actions = [trivialExample.ReproductionAction]
    ...
}
```

