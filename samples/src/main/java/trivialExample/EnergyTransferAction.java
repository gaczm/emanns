package trivialExample;

import com.google.common.collect.Iterables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.edu.agh.ap.action.Action;
import pl.edu.agh.ap.agent.Agent;

import java.util.Collection;
import java.util.Random;

public class EnergyTransferAction implements Action {

    private static final Logger logger = LoggerFactory.getLogger(EnergyTransferAction.class);
    
    @Override
    public void execute(Agent<?> parent, Collection<Agent<?>> agents) {
        @SuppressWarnings("unchecked")
        Collection<Agent<DummyAgent>> castedAgents = (Collection) agents;

        for (Agent<DummyAgent> agent : castedAgents) {
            Agent<DummyAgent> randomAgent = pickDifferentThan(castedAgents, agent);
            if (new Random().nextInt(2) == 0) {
                executeForSingleAgent(agent, randomAgent);
            }
        }
    }

    private <E> E pickDifferentThan(Collection<E> elements, E element) {
        E randomAgent;
        do {
            randomAgent = pick(elements);
        } while (randomAgent == element);
        return randomAgent;
    }

    private <E> E pick(Collection<E> elements) {
        return Iterables.get(elements, new Random().nextInt(elements.size()));
    }

    private void executeForSingleAgent(Agent<DummyAgent> agent, Agent<DummyAgent> otherAgent) {
        if (agent.getBehavior().getEnergy() > otherAgent.getBehavior().getEnergy()) {
            logger.info("Agent: " + agent.getName() + " have more energy than: " + otherAgent.getName());

            int otherAgentEnergy = otherAgent.getBehavior().getEnergy();
            int amountOfEnergyToTransfer = new Random().nextInt((int)(0.8*otherAgentEnergy) + 1);
            
            agent.getBehavior().setEnergy(agent.getBehavior().getEnergy() + amountOfEnergyToTransfer);
            otherAgent.getBehavior().setEnergy(otherAgent.getBehavior().getEnergy() - amountOfEnergyToTransfer);
            
            logger.info("Agent: " + agent.getName() + " have now: " + agent.getBehavior().getEnergy() + " energy points");
            logger.info("Agent: " + otherAgent.getName() + " have now: " + otherAgent.getBehavior().getEnergy() + " energy points");
        }
    }

}
