package trivialExample;

import pl.edu.agh.ap.agent.AgentBehavior;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class DummyAgent extends AgentBehavior {

    private int energy = new Random().nextInt(100_000);

    @Override
    public void doStep(int stepNumber) {
        heavyComputations();
    }

    private void heavyComputations() {
        try {
            TimeUnit.MILLISECONDS.sleep(50);
        } catch (InterruptedException ignored) {
            Thread.currentThread().interrupt();
        }
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }
}
