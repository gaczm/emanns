package trivialExample;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.edu.agh.ap.agent.Agent;

public class DeathAction extends pl.edu.agh.ap.action.DeathAction {

    private static final Logger logger = LoggerFactory.getLogger(DeathAction.class);
    
    @Override
    @SuppressWarnings("unchecked")
    protected boolean shouldDie(Agent<?> agent) {
        return DummyAgent.class.isAssignableFrom(agent.getBehaviorClass()) && checkEnergy((Agent<DummyAgent>) agent);
    }
    
    private boolean checkEnergy(Agent<DummyAgent> agent) {
        if (agent.getBehavior().getEnergy() == 0) {
            logger.debug("Agent: " + agent.getName() + " is about to die");
            return true;
        }
        return false;
    }

}
