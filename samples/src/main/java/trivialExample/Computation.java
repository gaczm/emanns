package trivialExample;

import ch.qos.logback.classic.BasicConfigurator;
import pl.edu.agh.ap.Platform;
import pl.edu.agh.ap.configuration.Configuration;
import pl.edu.agh.ap.configuration.ConfigurationLoader;

public class Computation {

    public static void main(String[] args) throws InterruptedException {
        BasicConfigurator.configureDefaultContext();
        
        Configuration configuration = ConfigurationLoader.loadFromClassPath("trivialExample/computation.cfg");
        Platform platform = new Platform(configuration);
        platform.run();
    }
    
}
