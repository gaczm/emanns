package trivialExample;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.edu.agh.ap.action.Action;
import pl.edu.agh.ap.agent.Agent;

import java.util.Collection;

public class WorkplaceStateCheckerAction implements Action {

    private static final Logger logger = LoggerFactory.getLogger(WorkplaceStateCheckerAction.class);

    @Override
    public void execute(Agent<?> parent, Collection<Agent<?>> agents) {
        logger.info("We have " + agents.size() + " agents currently");
    }
}
