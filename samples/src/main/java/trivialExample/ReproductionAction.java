package trivialExample;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.edu.agh.ap.action.Action;
import pl.edu.agh.ap.agent.Agent;
import pl.edu.agh.ap.agent.AgentBuilder;

import java.util.Collection;
import java.util.Random;

public class ReproductionAction implements Action {

    private static final Logger logger = LoggerFactory.getLogger(ReproductionAction.class);

    private Random random = new Random();

    @Override
    public void execute(Agent<?> parent, Collection<Agent<?>> agents) {
        @SuppressWarnings("unchecked")
        Collection<Agent<DummyAgent>> castedAgents = (Collection) agents;

        for (Agent<DummyAgent> agent : castedAgents) {
            if (random.nextInt(100) == 0) {
                Agent<DummyAgent> newAgent = AgentBuilder.baseOn(agent).build();
                int energyForNewAgent = agent.getBehavior().getEnergy() / 2;
                newAgent.getBehavior().setEnergy(energyForNewAgent);
                agent.getBehavior().setEnergy(agent.getBehavior().getEnergy() - energyForNewAgent);
                
                logger.info("Created new agent from: " + agent.getName());
                parent.addChild(newAgent);
            }
        }
    }

}
