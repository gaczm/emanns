package pl.edu.agh.ap.configuration;

import org.junit.Test;
import pl.edu.agh.ap.Platform;
import pl.edu.agh.ap.agent.AgentBehavior;

public class PlatformTest {

    public static class TrivialAgent extends AgentBehavior {
        @Override
        public void doStep(int stepNumber) {
            System.out.println(this + ": I'm doing step");
        }
    }

    @Test
    public void foo() throws InterruptedException {
        Configuration config = ConfigurationLoader.load(
                "def numberOfAgents = 10\n" +
                        "\n" +
                        "import pl.edu.agh.ap.configuration.PlatformTest.TrivialAgent\n" +
                        "\n" +
                        "configuration {\n" +
                        "    computationDurationInSeconds = 1\n" +
                        "    workplace {\n" +
                        "        (0..numberOfAgents).each { index ->\n" +
                        "            agent {\n" +
                        "                name = \"Agent $index\"\n" +
                        "                agentClass = TrivialAgent\n" +
                        "\n" +
                        "                agent {\n" +
                        "                    name = \"Agent $index.1\"\n" +
                        "                    agentClass = TrivialAgent\n" +
                        "\n" +
                        "                    agent {\n" +
                        "                        name = \"Agent $index.1.1\"\n" +
                        "                        agentClass = TrivialAgent\n" +
                        "\n" +
                        "                        agent {\n" +
                        "                            name = \"Agent $index.1.1.1\"\n" +
                        "                            agentClass = TrivialAgent\n" +
                        "                        }\n" +
                        "\n" +
                        "                        agent {\n" +
                        "                            name = \"Agent $index.1.1.2\"\n" +
                        "                            agentClass = TrivialAgent\n" +
                        "                        }\n" +
                        "\n" +
                        "                        agent {\n" +
                        "                            name = \"Agent $index.1.1.3\"\n" +
                        "                            agentClass = TrivialAgent\n" +
                        "                        }\n" +
                        "                    }\n" +
                        "                    agent {\n" +
                        "                        name = \"Agent $index.1.2\"\n" +
                        "                        agentClass = TrivialAgent\n" +
                        "\n" +
                        "                        agent {\n" +
                        "                            name = \"Agent $index.1.2.1\"\n" +
                        "                            agentClass = TrivialAgent\n" +
                        "                        }\n" +
                        "\n" +
                        "                        agent {\n" +
                        "                            name = \"Agent $index.1.2.2\"\n" +
                        "                            agentClass = TrivialAgent\n" +
                        "                        }\n" +
                        "\n" +
                        "                        agent {\n" +
                        "                            name = \"Agent $index.1.2.3\"\n" +
                        "                            agentClass = TrivialAgent\n" +
                        "                        }\n" +
                        "                    }\n" +
                        "                }\n" +
                        "            }\n" +
                        "        }\n" +
                        "    }\n" +
                        "}\n");
        Platform platform = new Platform(config);
        platform.run();
    }
}