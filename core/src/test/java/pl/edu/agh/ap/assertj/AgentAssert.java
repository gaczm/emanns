package pl.edu.agh.ap.assertj;

import org.assertj.core.api.AbstractAssert;
import org.assertj.core.api.Assertions;
import pl.edu.agh.ap.agent.Agent;

public class AgentAssert extends AbstractAssert<AgentAssert, Agent<?>> {

    public AgentAssert(Agent<?> actual) {
        super(actual, AgentAssert.class);
    }
    
    public static AgentAssert assertThat(Agent<?> actual) {
        return new AgentAssert(actual);
    }
    
    public AgentAssert hasName(String name) {
        Assertions.assertThat(actual.getName()).describedAs("Name is different").isEqualTo(name);
        return this;
    }

    public AgentAssert numberOfChildrenEquals(int number) {
        Assertions.assertThat(actual.getChildren()).describedAs("Different number of children").hasSize(number);
        return this;
    }

}
