package pl.edu.agh.ap;

import org.junit.Test;
import pl.edu.agh.ap.agent.Agent;
import pl.edu.agh.ap.agent.AgentBehavior;
import pl.edu.agh.ap.agent.AgentBuilder;
import pl.edu.agh.ap.agent.Workplace;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;

import static org.assertj.core.api.Assertions.assertThat;

public class QueryTest {

    static class AgentWithMagicNumber extends AgentBehavior {
        
        private int magicNumber;
        private Iterator<Integer> sequence = Collections.<Integer>emptyList().iterator();

        @Override
        public void doStep(int stepNumber) {
            if (sequence.hasNext()) 
                magicNumber = sequence.next();
        }
        
        public void setMagicNumberSequence(Integer... sequence) {
            this.sequence = Arrays.asList(sequence).iterator();
        }

        public int getMagicNumber() {
            return magicNumber;
        }
    }
    
    static class QueryingAgent extends AgentBehavior {

        private AgentWithMagicNumber agentWithGreatestNumber;

        @Override
        public void doStep(int stepNumber) {
            agentWithGreatestNumber = query(AgentWithMagicNumber.class)
                    .max((a1, a2) -> a1.getMagicNumber() - a2.getMagicNumber())
                    .get();
        }

        public AgentWithMagicNumber getAgentWithGreatestNumber() {
            return agentWithGreatestNumber;
        }
    }

    @Test
    public void querying_shuld_work_in_basic_case() {
        Workplace workplace = new Workplace("", Collections.emptyList());
        
        Agent<AgentWithMagicNumber> agent1 = AgentBuilder.create(AgentWithMagicNumber.class);
        agent1.getBehavior().setMagicNumberSequence(1, 2, 3);
        workplace.addChild(agent1);
        agent1.setParent(workplace);

        Agent<AgentWithMagicNumber> agent2 = AgentBuilder.create(AgentWithMagicNumber.class);
        agent2.getBehavior().setMagicNumberSequence(10, 20, 1);
        workplace.addChild(agent2);
        agent2.setParent(workplace);

        Agent<AgentWithMagicNumber> agent3 = AgentBuilder.create(AgentWithMagicNumber.class);
        agent3.getBehavior().setMagicNumberSequence(100, 0, 0);
        workplace.addChild(agent3);
        agent3.setParent(workplace);

        Agent<QueryingAgent> agent4 = AgentBuilder.create(QueryingAgent.class);
        workplace.addChild(agent4);
        agent4.setParent(workplace);
        
        workplace.getBehavior().doStep(0);
        assertThat(agent4.getBehavior().getAgentWithGreatestNumber()).isSameAs(agent3.getBehavior());
        workplace.getBehavior().doStep(1);
        assertThat(agent4.getBehavior().getAgentWithGreatestNumber()).isSameAs(agent2.getBehavior());
        workplace.getBehavior().doStep(2);
        assertThat(agent4.getBehavior().getAgentWithGreatestNumber()).isSameAs(agent1.getBehavior());
    }

}
