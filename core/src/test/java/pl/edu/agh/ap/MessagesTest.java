package pl.edu.agh.ap;

import org.junit.Ignore;
import org.junit.Test;
import pl.edu.agh.ap.agent.*;
import pl.edu.agh.ap.exception.AgentInstantiationException;
import pl.edu.agh.ap.message.Message;
import pl.edu.agh.ap.message.MessageHandler;

import java.util.Collections;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

public class MessagesTest {

    public static final String PAYLOAD = "hello";

    static class AgentWithInvalidHandler extends AgentBehavior {

        @Override
        public void doStep(int stepNumber) {
            sendMessage(new Message(agent -> true, PAYLOAD));
        }

        @MessageHandler
        public void invalidHandlerWithoutArgs() {
        }
    }

    static class AgentWithValidHandler extends AgentBehavior {

        private Optional<String> received = Optional.empty();

        @Override
        public void doStep(int stepNumber) {
            sendMessage(new Message(agent -> true, PAYLOAD));
        }

        @MessageHandler
        public void myHandler(String arg) {
            received = Optional.of(arg);
        }

        public Optional<String> getReceived() {
            return received;
        }
    }

    static class AgentWithMultipleHandlers extends AgentBehavior {

        private Optional<String> receivedBy1 = Optional.empty();
        private Optional<Integer> receivedBy2 = Optional.empty();
        private Optional<Object> receivedBy3 = Optional.empty();

        @Override
        public void doStep(int stepNumber) {
            sendMessage(new Message(agent -> true, PAYLOAD));
        }

        @MessageHandler
        public void myHandler1(String arg) {
            receivedBy1 = Optional.of(arg);
        }

        @MessageHandler
        public void myHandler2(Integer arg) {
            receivedBy2 = Optional.of(arg);
        }

        @MessageHandler
        public void myHandler3(Object arg) {
            receivedBy3 = Optional.of(arg);
        }

        public Optional<String> getReceivedBy1() {
            return receivedBy1;
        }

        public Optional<Integer> getReceivedBy2() {
            return receivedBy2;
        }

        public Optional<Object> getReceivedBy3() {
            return receivedBy3;
        }
    }

    @Ignore("Łukasz, weź to napisz")
    @Test(expected = AgentInstantiationException.class)
    public void should_fail_on_invalid_handler() {
        AgentBuilder.create(AgentWithInvalidHandler.class);
    }

    @Ignore("Łukasz, weź to napisz")
    @Test
    public void should_receive_message_correctly() {
        Workplace parent = new Workplace("", Collections.emptyList());
        Agent<AgentWithValidHandler> agent = AgentBuilder.create(AgentWithValidHandler.class);
        parent.addChild(agent);
        agent.setParent(parent);
        parent.getBehavior().doStep(0);
        
        assertThat(agent.getBehavior().getReceived().isPresent()).isTrue();
        assertThat(agent.getBehavior().getReceived().get()).isEqualTo(PAYLOAD);
    }

    @Ignore("Łukasz, weź to napisz")
    @Test
    public void should_pass_message_to_valid_handlers() {
        Workplace parent = new Workplace("", Collections.emptyList());
        Agent<AgentWithMultipleHandlers> agent = AgentBuilder.create(AgentWithMultipleHandlers.class);
        parent.addChild(agent);
        agent.setParent(parent);
        parent.getBehavior().doStep(0);

        assertThat(agent.getBehavior().getReceivedBy1().isPresent()).isTrue();
        assertThat(agent.getBehavior().getReceivedBy1().get()).isEqualTo(PAYLOAD);
        assertThat(agent.getBehavior().getReceivedBy2().isPresent()).isFalse();
        assertThat(agent.getBehavior().getReceivedBy3().isPresent()).isTrue();
        assertThat(agent.getBehavior().getReceivedBy3().get()).isEqualTo(PAYLOAD);
    }
    
}
