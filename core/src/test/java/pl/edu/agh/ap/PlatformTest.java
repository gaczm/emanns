package pl.edu.agh.ap;

import org.junit.Test;
import pl.edu.agh.ap.action.Action;
import pl.edu.agh.ap.agent.Agent;
import pl.edu.agh.ap.agent.AgentBehavior;
import pl.edu.agh.ap.assertj.AgentAssert;
import pl.edu.agh.ap.configuration.AgentDescriptor;
import pl.edu.agh.ap.configuration.Configuration;
import pl.edu.agh.ap.configuration.StopCondition;
import pl.edu.agh.ap.configuration.WorkplaceDescriptor;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.Arrays.asList;
import static pl.edu.agh.ap.assertj.Assertions.assertThat;

public class PlatformTest {
    
    public static class DummyAgent extends AgentBehavior {
        @Override
        public void doStep(int stepNumber) {
        }
    }
    
    static class SimpleDescriptor implements AgentDescriptor {

        private final String name;
        private Optional<AgentDescriptor> parent;
        private List<AgentDescriptor> children = Collections.emptyList();

        public SimpleDescriptor(String name) {
            this.name = name;
        }

        @Override
        public Class<? extends AgentBehavior> getAgentClass() {
            return DummyAgent.class;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public Optional<AgentDescriptor> getParent() {
            return parent;
        }

        @Override
        public List<AgentDescriptor> getChildren() {
            return children;
        }

        @Override
        public Map<String, Object> getSettings() {
            return Collections.emptyMap();
        }

        @Override
        public List<Class<Action>> getActions() {
            return Collections.emptyList();
        }

        public void setParent(AgentDescriptor parent) {
            this.parent = Optional.ofNullable(parent);
        }

        public void setChildren(List<AgentDescriptor> children) {
            this.children = children;
        }
    }
    
    @Test
    public void should_instantiate_agents_correctly() {
        Platform platform = new Platform(new Configuration() {

            /*
            simple hierarchy:
                root
               /    \
              a      b
             / \    / \
            a1 a2  b1  b2
            */
            @Override
            public List<WorkplaceDescriptor> getWorkplaces() {
                return Collections.singletonList(new WorkplaceDescriptor() {
                    @Override
                    public List<AgentDescriptor> getAgents() {
                        SimpleDescriptor root = new SimpleDescriptor("root");
                        SimpleDescriptor a = new SimpleDescriptor("a");
                        SimpleDescriptor b = new SimpleDescriptor("b");
                        SimpleDescriptor a1 = new SimpleDescriptor("a1");
                        SimpleDescriptor a2 = new SimpleDescriptor("a2");
                        SimpleDescriptor b1 = new SimpleDescriptor("b1");
                        SimpleDescriptor b2 = new SimpleDescriptor("b2");

                        root.setChildren(asList(a, b));
                        a.setParent(root);
                        b.setParent(root);

                        a.setChildren(asList(a1, a2));
                        a1.setParent(a);
                        a2.setParent(a);

                        b.setChildren(asList(b1, b2));
                        b1.setParent(b);
                        b2.setParent(b);

                        return asList(root);
                    }

                    @Override
                    public List<Class<Action>> getActions() {
                        return Collections.emptyList();
                    }
                });
            }

            @Override
            public StopCondition getStopCondition() {
                return null;
            }
        });

        List<Agent<?>> agents = platform.getWorkplaces().get(0).getChildren();
        assertThat(agents).hasSize(1);

        Agent<?> root = agents.get(0);

        AgentAssert.assertThat(root)
                .hasName("root")
                .numberOfChildrenEquals(2);

        Agent<?> a = root.getChildren().get(0);
        AgentAssert.assertThat(a)
                .hasName("a")
                .numberOfChildrenEquals(2);
        
        Agent<?> a1 = a.getChildren().get(0);
        AgentAssert.assertThat(a1)
                .hasName("a1")
                .numberOfChildrenEquals(0);

        Agent<?> a2 = a.getChildren().get(1);
        AgentAssert.assertThat(a2)
                .hasName("a2")
                .numberOfChildrenEquals(0);
        
        Agent<?> b = root.getChildren().get(1);
        AgentAssert.assertThat(b)
                .hasName("b")
                .numberOfChildrenEquals(2);

        Agent<?> b1 = b.getChildren().get(0);
        AgentAssert.assertThat(b1)
                .hasName("b1")
                .numberOfChildrenEquals(0);

        Agent<?> b2 = b.getChildren().get(1);
        AgentAssert.assertThat(b2)
                .hasName("b2")
                .numberOfChildrenEquals(0);
    }
    
}