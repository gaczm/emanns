package pl.edu.agh.ap.configuration;

import java.util.List;

public interface Configuration {

    List<WorkplaceDescriptor> getWorkplaces();
    StopCondition getStopCondition();
    
}
