package pl.edu.agh.ap;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.edu.agh.ap.agent.*;
import pl.edu.agh.ap.agent.internal.InternalAgentRepresentation;
import pl.edu.agh.ap.configuration.AgentDescriptor;
import pl.edu.agh.ap.configuration.Configuration;
import pl.edu.agh.ap.configuration.WorkplaceDescriptor;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static pl.edu.agh.ap.misc.TimeMeasurement.measureTime;

/**
 * Main class for running agents platform
 */
public class Platform {

    private static final Logger logger = LoggerFactory.getLogger(Platform.class);

    private final Configuration configuration;

    private final List<Workplace> workplaces;

    public Platform(Configuration configuration) {
        this.configuration = configuration;
        workplaces = measureTime(() -> instantiateWorkplaces(configuration.getWorkplaces()), "Workplaces created in: ");
    }

    private List<Workplace> instantiateWorkplaces(List<WorkplaceDescriptor> workplaces) {
        ImmutableList.Builder<Workplace> builder = ImmutableList.builder();
        for (int i = 0; i < workplaces.size(); i++) {
            WorkplaceDescriptor desc = workplaces.get(i);
            Workplace workplace = new Workplace("workplace-" + i, desc.getActions());
            workplace.addChildren(instantiateAgents(workplace, desc.getAgents()));
            builder.add(workplace);
        }
        return builder.build();
    }

    public void run() throws InterruptedException {
        List<Thread> threads = createThreadsForWorkplaces();
        waitUntilStopConditionReached();
        stopWorkplaces(threads);
    }

    private List<Thread> createThreadsForWorkplaces() {
        List<Thread> threads = new ArrayList<>(workplaces.size());
        for (int workplaceIndex = 0; workplaceIndex < workplaces.size(); workplaceIndex++) {
            createWorkplaceThread(threads, workplaceIndex);
        }
        return threads;
    }

    private void createWorkplaceThread(List<Thread> threads, int workplaceIndex) {
        Workplace workplace = workplaces.get(workplaceIndex);
        Thread thread = new Thread(workplace);
        thread.setName("Workplace-" + workplaceIndex);
        thread.start();
        threads.add(thread);
    }

    private void waitUntilStopConditionReached() {
        while (!configuration.getStopCondition().isReached()) {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                logger.warn("Interrupted", e);
            }
        }
    }

    private void stopWorkplaces(List<Thread> threads) throws InterruptedException {
        logger.info("Stopping workplaces");
        for (Thread thread : threads) {
            thread.interrupt();
            thread.join(10_000); // wait up to 10 seconds for thread
        }
        logger.info("Workplaces stopped");
    }

    private List<Agent<?>> instantiateAgents(Workplace workplace, List<AgentDescriptor> agentsDescriptors) {
        // here we are creating agents using provided descriptions
        return agentsDescriptors.stream()
                .map(desc -> buildAgent(desc, workplace))
                .collect(Collectors.toList());
    }

    private Agent<?> buildAgent(AgentDescriptor descriptor, Agent<?> parent) {
        Agent<?> agent = AgentBuilder.builder(descriptor.getAgentClass())
                .withActions(descriptor.getActions())
                .withSettings(descriptor.getSettings())
                .withParent(parent)
                .withName(descriptor.getName())
                .build();

        InternalAgentRepresentation internalAgentRepresentation = (InternalAgentRepresentation) agent;
        descriptor.getChildren()
                .stream()
                .map(childDescriptor -> buildAgent(childDescriptor, agent))
                .forEach(internalAgentRepresentation::addChild);

        return agent;
    }

    @VisibleForTesting
    List<Workplace> getWorkplaces() {
        return workplaces;
    }
}
