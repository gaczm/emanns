package pl.edu.agh.ap.configuration;

import pl.edu.agh.ap.action.Action;
import pl.edu.agh.ap.agent.AgentBehavior;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface AgentDescriptor {
    
    Class<? extends AgentBehavior> getAgentClass();
    String getName();
    Optional<AgentDescriptor> getParent();
    List<AgentDescriptor> getChildren();
    Map<String, Object> getSettings();
    List<Class<Action>> getActions();
    
}
