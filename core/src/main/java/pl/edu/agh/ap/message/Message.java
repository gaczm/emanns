package pl.edu.agh.ap.message;

import pl.edu.agh.ap.agent.AgentBehavior;

import java.io.Serializable;
import java.util.function.Predicate;

public class Message {

    private final Predicate<AgentBehavior> recipientSelectingPredicate;
    private final Serializable payload;

    public Message(Predicate<AgentBehavior> recipientSelectingPredicate, Serializable payload) {
        this.recipientSelectingPredicate = recipientSelectingPredicate;
        this.payload = payload;
    }

    public Predicate<AgentBehavior> getPredicate() {
        return recipientSelectingPredicate;
    }

    public Serializable getPayload() {
        return payload;
    }
}
