package pl.edu.agh.ap.misc;

import com.google.common.base.Stopwatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

public class TimeMeasurement {

    private static final Logger logger = LoggerFactory.getLogger(TimeMeasurement.class);

    public static <T> T measureTime(Supplier<T> func, String messageToLog) {
        Stopwatch stopwatch = Stopwatch.createStarted();
        try {
            return func.get();
        } finally {
            logger.debug(String.format("%s %.2fs", messageToLog, (double) stopwatch.elapsed(TimeUnit.MILLISECONDS) / 1000));
        }
    }
}
