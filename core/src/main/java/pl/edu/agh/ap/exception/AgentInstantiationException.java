package pl.edu.agh.ap.exception;

public class AgentInstantiationException extends RuntimeException {

    public AgentInstantiationException(String message) {
        super(message);
    }

    public AgentInstantiationException(String message, Throwable cause) {
        super(message, cause);
    }

    public AgentInstantiationException(Throwable cause) {
        super(cause);
    }
}
