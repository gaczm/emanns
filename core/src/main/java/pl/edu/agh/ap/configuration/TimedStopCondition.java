package pl.edu.agh.ap.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.time.LocalDateTime;

public class TimedStopCondition implements StopCondition {

    private static final Logger logger = LoggerFactory.getLogger(TimedStopCondition.class);

    private final Duration desiredDuration;
    private final LocalDateTime startedAt = LocalDateTime.now();

    public TimedStopCondition(Duration desiredDuration) {
        this.desiredDuration = desiredDuration;
    }

    @Override
    public boolean isReached() {
        if (Duration.between(startedAt, LocalDateTime.now()).toMillis() >= desiredDuration.toMillis()) {
            logger.info("Time's up");
            return true;
        }
        return false;
    }
}
