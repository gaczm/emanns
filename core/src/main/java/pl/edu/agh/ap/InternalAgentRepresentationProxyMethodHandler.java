package pl.edu.agh.ap;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableMap;
import javassist.util.proxy.MethodHandler;
import pl.edu.agh.ap.agent.Agent;
import pl.edu.agh.ap.agent.internal.InternalAgentRepresentation;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Map;

public class InternalAgentRepresentationProxyMethodHandler implements MethodHandler {

    private final Map<String, MethodImplementation> reimplementedMethods;

    public InternalAgentRepresentationProxyMethodHandler(InternalAgentRepresentation internalAgentRepresentation) throws InvocationTargetException, IllegalAccessException {
        reimplementedMethods = reimplementedMethods(internalAgentRepresentation);
    }

    @Override
    public Object invoke(Object self, Method overridden, Method forwarder, Object[] args) throws Throwable {
        MethodImplementation methodImplementation = reimplementedMethods.get(method(overridden.getName(), overridden.getParameterTypes()));
        if (methodImplementation != null) {
            return methodImplementation.execute(self, forwarder, args);
        }
        return forwarder.invoke(self, args);
    }

    private static ImmutableMap<String, MethodImplementation> reimplementedMethods(InternalAgentRepresentation enhancedAgent) throws IllegalAccessException, InvocationTargetException {
        final Void VOID = null;
        return ImmutableMap.<String, MethodImplementation>builder()
                .put(method("doStep", int.class), (self, overriddenMethod, args) -> {
                    overriddenMethod.invoke(self, args);
                    enhancedAgent.doStepOnChildren((Integer) args[0]);
                    return VOID;
                })
                .put(method("addChild", Agent.class), (self, overriddenMethod, args) -> {
                    enhancedAgent.addChild((Agent) args[0]);
                    return VOID;
                })
                .put(method("removeChild", Agent.class), (self, overriddenMethod, args) -> {
                    enhancedAgent.removeChild((Agent) args[0]);
                    return VOID;
                })
                .put(method("getChildren"), (self, overriddenMethod, args) -> enhancedAgent.getChildren())
                .put(method("setParent", Agent.class), (self, overriddenMethod, args) -> {
                    enhancedAgent.setParent((Agent) args[0]);
                    return VOID;
                })
                .put(method("getParent"), (self, overriddenMethod, args) -> enhancedAgent.getParent())
                .put(method("getActionsTypes"), (self, overriddenMethod, args) -> enhancedAgent.getActionsTypes())
                .put(method("getSettings"), (self, overriddenMethod, args) -> enhancedAgent.getSettings())
                .put(method("getName"), (self, overriddenMethod, args) -> enhancedAgent.getName())
                .put(method("getBehavior"), (self, overriddenMethod, args) -> enhancedAgent.getBehavior())
                .put(method("getBehaviorClass"), (self, overriddenMethod, args) -> enhancedAgent.getBehaviorClass())
                .put(method("query"), (self, overriddenMethod, args) -> enhancedAgent.query())
                .build();
    }

    private static interface MethodImplementation {
        Object execute(Object self, Method overridden, Object[] args) throws InvocationTargetException, IllegalAccessException;
    }

    private static String method(String name, Class<?>... args) {
        return name + "(" + Joiner.on(",").join(args);
    }
}
