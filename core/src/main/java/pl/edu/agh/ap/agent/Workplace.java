package pl.edu.agh.ap.agent;

import com.google.common.collect.ImmutableList;
import pl.edu.agh.ap.action.Action;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Workplace implements Agent<Workplace.WorkplaceBehavior>, Runnable {

    private final String name;
    private final List<Action> actions;

    public Workplace(String name, List<Class<Action>> actions) {
        this.name = name;
        this.actions = actions.stream()
                .map(AgentUtils::instantiateSafely)
                .collect(Collectors.toList());
    }

    public class WorkplaceBehavior extends AgentBehavior {
        @Override
        public void doStep(int stepNumber) {
            for (Agent agent : getChildren()) {
                agent.getBehavior().doStep(stepNumber);
            }

            actions.forEach(action -> action.execute(Workplace.this, ImmutableList.copyOf(children)));
            removeAgents();
        }
    }

    private void removeAgents() {
        agentsForRemovalAtTheEndOfTurn.forEach(children::remove);
        agentsForRemovalAtTheEndOfTurn.clear();
    }

    private final WorkplaceBehavior behavior = new WorkplaceBehavior();
    private final List<Agent<?>> children = new LinkedList<>();
    private final List<Agent> agentsForRemovalAtTheEndOfTurn = new LinkedList<>();

    @Override
    public List<Agent<?>> getChildren() {
        return children;
    }

    @Override
    public void addChild(Agent<?> child) {
        children.add(child);
    }

    @Override
    public void addChildren(Collection<Agent<?>> children) {
        this.children.addAll(children);
    }

    @Override
    public void removeChild(Agent<?> child) {
        agentsForRemovalAtTheEndOfTurn.add(child);
    }

    @Override
    public void setParent(Agent parent) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Agent getParent() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public WorkplaceBehavior getBehavior() {
        return behavior;
    }

    @Override
    public Class<WorkplaceBehavior> getBehaviorClass() {
        return WorkplaceBehavior.class;
    }

    @Override
    public void run() {
        int step = 1;
        while (!Thread.currentThread().isInterrupted()) {
            getBehavior().doStep(step++);
        }
    }

}
