package pl.edu.agh.ap.agent;

import java.util.Collection;
import java.util.List;

public interface Agent<A extends AgentBehavior> {

    void addChild(Agent<?> child);
    void addChildren(Collection<Agent<?>> children);
    void removeChild(Agent<?> child);
    List<Agent<?>> getChildren();
    
    void setParent(Agent<?> parent);
    Agent<?> getParent();
    
    String getName();
    
    A getBehavior();
    Class<A> getBehaviorClass(); 

}
