package pl.edu.agh.ap.action;

import pl.edu.agh.ap.agent.Agent;

import java.util.Collection;

public interface Action {

    void execute(Agent<?> parent, Collection<Agent<?>> agents);
    
}
