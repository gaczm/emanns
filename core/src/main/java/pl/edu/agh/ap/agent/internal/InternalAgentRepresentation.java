package pl.edu.agh.ap.agent.internal;

import pl.edu.agh.ap.action.Action;
import pl.edu.agh.ap.agent.Agent;
import pl.edu.agh.ap.agent.AgentBehavior;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * This interface represents internal structure of Agent.
 * 
 * While setting up platform for new computation, we connect AgentBehaviors (created by user) 
 * with Agent class in {@link pl.edu.agh.ap.agent.AgentBuilder} using proxy. This class shouldn't be used
 * by end user and it should event be used in platform internal code unless really needed. 
 */
public interface InternalAgentRepresentation extends Agent<AgentBehavior> {

    void doStepOnChildren(int stepNumber);
    Map<String, Object> getSettings();
    Stream<AgentBehavior> query();
    List<Class<Action>> getActionsTypes();
    
}
