package pl.edu.agh.ap.action;

import pl.edu.agh.ap.agent.AgentBehavior;
import pl.edu.agh.ap.agent.Agent;

public interface DeprecatedAction {

    boolean executionPossible(AgentBehavior agent);
    void execute(Agent<?> agent);
    
}
