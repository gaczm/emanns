package pl.edu.agh.ap.configuration;

@FunctionalInterface
public interface StopCondition {
    
    boolean isReached();
    
}
