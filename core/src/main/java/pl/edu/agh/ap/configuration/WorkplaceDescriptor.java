package pl.edu.agh.ap.configuration;

import pl.edu.agh.ap.action.Action;

import java.util.List;

public interface WorkplaceDescriptor {
    List<AgentDescriptor> getAgents();
    List<Class<Action>> getActions();
}
