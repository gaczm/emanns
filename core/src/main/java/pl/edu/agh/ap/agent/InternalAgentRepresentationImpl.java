package pl.edu.agh.ap.agent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.edu.agh.ap.action.Action;
import pl.edu.agh.ap.agent.internal.InternalAgentRepresentation;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class InternalAgentRepresentationImpl implements InternalAgentRepresentation {

    private static final Logger logger = LoggerFactory.getLogger(InternalAgentRepresentationImpl.class);

    private final List<Agent<?>> children = new LinkedList<>();
    private final String name;
    private final Map<String, Object> settings;
    private final List<Action> actions;
    
    private Agent parent;
    private AgentBehavior self;
    private List<Agent> agentsForRemovalAtTheEndOfTurn = new LinkedList<>();
    private List<Class<Action>> actionsTypes;

    public InternalAgentRepresentationImpl(List<Class<Action>> actionsTypes, Map<String, Object> settings, Agent<?> parent, String name) {
        this.actionsTypes = actionsTypes;
        this.actions = actionsTypes.stream()
                .map(AgentUtils::instantiateSafely)
                .collect(Collectors.toList());
        this.settings = settings;
        this.parent = parent;
        this.name = name != null ? name : AgentUtils.randomName();
    }

    public void doStepOnChildren(int stepNumber) {
        children.forEach(child -> child.getBehavior().doStep(stepNumber));
        executeActions();
        removeAgents();
    }

    private void executeActions() {
        actions.forEach(action -> action.execute(this, children));
    }

    private void removeAgents() {
        agentsForRemovalAtTheEndOfTurn.forEach(children::remove);
        agentsForRemovalAtTheEndOfTurn.clear();
    }

    @Override
    public void addChild(Agent<?> child) {
        children.add(child);
    }

    @Override
    public void removeChild(Agent<?> child) {
        agentsForRemovalAtTheEndOfTurn.add(child);
    }

    @Override
    public void addChildren(Collection<Agent<?>> children) {
        this.children.addAll(children);
    }

    @Override
    public List<Agent<?>> getChildren() {
        return children;
    }

    @Override
    public void setParent(Agent<?> parent) {
        this.parent = parent;
    }

    @Override
    public Agent<?> getParent() {
        return parent;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Stream<AgentBehavior> query() {
        return getParent().getChildren().stream().map(a -> a.getBehavior());
    }

    @Override
    public List<Class<Action>> getActionsTypes() {
        return actionsTypes;
    }

    public Map<String, Object> getSettings() {
        return settings;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof InternalAgentRepresentation)) return false;
        InternalAgentRepresentationImpl that = (InternalAgentRepresentationImpl) o;
        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    /**
     * Self is an proxy over Agent and EnhancedAgent
     */
    public void setSelf(AgentBehavior self) {
        this.self = self;
    }

    @Override
    public AgentBehavior getBehavior() {
        return self;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Class<AgentBehavior> getBehaviorClass() {
        Class clazz = self.getClass();
        while (clazz != null && clazz.getName().contains("_$$_jvst")) {
            clazz = clazz.getSuperclass();
        }
        return clazz;
    }

}
