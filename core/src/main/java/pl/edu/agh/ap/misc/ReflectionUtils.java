package pl.edu.agh.ap.misc;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

public class ReflectionUtils {
    
    public static Collection<Method> allMethodsAnnotatedBy(Class<?> clazz, Class<? extends Annotation> annotation) {
        Set<Method> methods = new LinkedHashSet<>();
        for (Method method : clazz.getMethods()) {
            if (method.getAnnotation(annotation) != null)
                methods.add(method);
        }
        for (Method method : clazz.getDeclaredMethods()) {
            if (method.getAnnotation(annotation) != null)
                methods.add(method);
        }
        return methods;
    }
    
}
