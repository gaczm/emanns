package pl.edu.agh.ap.action;

import pl.edu.agh.ap.agent.Agent;

import java.util.Collection;

public abstract class DeathAction implements Action {

    @Override
    public void execute(Agent<?> parent, Collection<Agent<?>> agents) {
        agents.stream()
                .filter(this::shouldDie)
                .forEach(parent::removeChild);
    }

    protected abstract boolean shouldDie(Agent<?> agent);
}
