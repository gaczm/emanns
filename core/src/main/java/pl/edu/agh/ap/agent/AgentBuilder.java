package pl.edu.agh.ap.agent;

import javassist.util.proxy.ProxyFactory;
import javassist.util.proxy.ProxyObject;
import pl.edu.agh.ap.InternalAgentRepresentationProxyMethodHandler;
import pl.edu.agh.ap.action.Action;
import pl.edu.agh.ap.agent.internal.InternalAgentRepresentation;
import pl.edu.agh.ap.exception.AgentInstantiationException;
import pl.edu.agh.ap.message.MessageHandler;

import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static pl.edu.agh.ap.misc.ReflectionUtils.allMethodsAnnotatedBy;

public class AgentBuilder {
    
    public static <A extends AgentBehavior> Builder<A> builder(Class<A> agentClass) {
        return new Builder<>(agentClass);
    }
    
    public static <A extends AgentBehavior> Builder<A> baseOn(Agent<A> existingAgent) {
        return baseOn(existingAgent.getBehavior());
    }
    
    @SuppressWarnings("unchecked")
    public static <A extends AgentBehavior> Builder<A> baseOn(A existingAgent) {
        InternalAgentRepresentation internalAgentRepresentation = (InternalAgentRepresentation) existingAgent;
        return (Builder<A>) builder(internalAgentRepresentation.getBehaviorClass())
                .withActions(internalAgentRepresentation.getActionsTypes())
                .withParent(internalAgentRepresentation.getParent())
                .withSettings(internalAgentRepresentation.getSettings());
    }

    public static class Builder<A extends AgentBehavior> {

        private Class<A> agentClass;
        private Map<String, Object> settings = Collections.emptyMap();
        private List<Class<Action>> actions = Collections.emptyList();
        private Agent<?> parent;
        private String name;

        public Builder(Class<A> agentClass) {
            this.agentClass = agentClass;
        }

        public Builder<A> withSettings(Map<String, Object> settings) {
            this.settings = settings;
            return this;
        }

        public Builder<A> withActions(List<Class<Action>> actions) {
            this.actions = actions;
            return this;
        }

        public Builder<A> withParent(Agent<?> parent) {
            this.parent = parent;
            return this;
        }

        public Builder<A> withName(String name) {
            this.name = name;
            return this;
        }

        public Agent<A> build() {
            return AgentBuilder.create(agentClass, settings, actions, parent, name);
        }
    }

    @Deprecated
    public static <A extends AgentBehavior> Agent<A> create(Class<A> agentClass) {
        return create(agentClass, Collections.emptyMap(), Collections.emptyList(), null, null);
    }

    private static <A extends AgentBehavior> Agent<A> create(Class<? extends AgentBehavior> agentClass, Map<String, Object> settings, List<Class<Action>> actions, Agent<?> parent, String name) {
        InternalAgentRepresentationImpl enhancedAgent = new InternalAgentRepresentationImpl(actions, settings, parent, name);
        
        try {
            verifyClassCorrectness(agentClass);
            Class clazz = prepareClassForThisAgent(agentClass);
            Object instance = clazz.newInstance();
            ((ProxyObject) instance).setHandler(new InternalAgentRepresentationProxyMethodHandler(enhancedAgent));

            @SuppressWarnings("unchecked")
            A behavior = (A) instance;
            enhancedAgent.setSelf(behavior);

            @SuppressWarnings("unchecked")
            Agent<A> agent = (Agent<A>) instance;
            return agent;
        } catch (Throwable e) {
            throw new AgentInstantiationException(e);
        }
    }

    private static <A extends AgentBehavior> void verifyClassCorrectness(Class<A> agentClass) {
        verifyMessageHandlers(agentClass);
    }

    private static <A extends AgentBehavior> void verifyMessageHandlers(Class<A> agentClass) {
        for (Method declaredHandler : allMethodsAnnotatedBy(agentClass, MessageHandler.class)) {
            if (declaredHandler.getParameterCount() != 1) {
                throw new AgentInstantiationException(MessageFormat.format("Invalid message handler: `{0}` declared in class: `{1}`", declaredHandler.getName(), declaredHandler.getDeclaringClass().getName()));
            }
        }
    }
    
    private static <A extends AgentBehavior> Class prepareClassForThisAgent(Class<A> agentClass) {
        ProxyFactory factory = new ProxyFactory();
        factory.setSuperclass(agentClass);
        factory.setInterfaces(new Class[]{InternalAgentRepresentation.class});
        return factory.createClass();
    }

}
