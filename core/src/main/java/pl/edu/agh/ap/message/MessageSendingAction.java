package pl.edu.agh.ap.message;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import pl.edu.agh.ap.action.DeprecatedAction;
import pl.edu.agh.ap.agent.AgentBehavior;
import pl.edu.agh.ap.agent.Agent;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import static pl.edu.agh.ap.misc.ReflectionUtils.allMethodsAnnotatedBy;

@Deprecated
public class MessageSendingAction implements DeprecatedAction {

    private static final LoadingCache<Class<? extends AgentBehavior>, Collection<Method>> methodsHandlersCache = CacheBuilder.newBuilder()
            .build(new CacheLoader<Class<? extends AgentBehavior>, Collection<Method>>() {
                @Override
                public Collection<Method> load(Class<? extends AgentBehavior> key) throws Exception {
                    return allMethodsAnnotatedBy(key, MessageHandler.class);
                }
            });

    private final Message message;

    public MessageSendingAction(Message message) {
        this.message = message;
    }

    @Override
    public boolean executionPossible(AgentBehavior agent) {
        return message.getPredicate().test(agent);
    }

    @Override
    public void execute(Agent<?> agent) {
        try {
            Serializable payload = message.getPayload();
            Collection<Method> handlers = findHandlers(agent.getBehaviorClass(), payload.getClass());
            deliverMessage(agent, payload, handlers);
        } catch (IllegalAccessException | InvocationTargetException | ExecutionException e) {
            throw new MessageDeliveryException("Problems with receiving message", e);
        }
    }

    private Collection<Method> findHandlers(Class<? extends AgentBehavior> agentType, Class<? extends Serializable> payloadType) throws ExecutionException {
        return methodsHandlersCache.get(agentType).stream()
                .filter(h -> h.getParameterTypes()[0].isAssignableFrom(payloadType))
                .collect(Collectors.toList());
    }

    private void deliverMessage(Agent<?> agent, Serializable payload, Collection<Method> handlers) throws IllegalAccessException, InvocationTargetException {
        if (handlers.isEmpty()) {
            throw new MessageDeliveryException("No handlers for type: " + payload.getClass() + " found");
        }

        for (Method handler : handlers) {
            handler.setAccessible(true);
            handler.invoke(agent.getBehavior(), payload);
        }
    }

}
