package pl.edu.agh.ap.agent;

import pl.edu.agh.ap.message.Message;

import java.util.Collections;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Class that describes agent behavior
 */
public abstract class AgentBehavior {

    /**
     * Step logic
     */
    public abstract void doStep(int stepNumber);

    protected Map<String, Object> getSettings() {
        // nothing here, this method is enhanced in {@link pl.edu.agh.ap.agent.EnhancedAgent}
        return Collections.emptyMap();
    }

    protected final void sendMessage(Message message) {
        // TODO
    }

    public Stream<AgentBehavior> query() {
        // nothing here, this method is enhanced in {@link pl.edu.agh.ap.agent.EnhancedAgent}
        return null;
    }

    public <B extends AgentBehavior> Stream<B> query(Class<B> queryLimiter) {
        return query()
                .filter(queryLimiter::isInstance)
                .map(queryLimiter::cast);
    }

}

