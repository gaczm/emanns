package pl.edu.agh.ap.agent;

import com.google.common.base.Stopwatch;

import java.util.UUID;

class AgentUtils {

    static String randomName() {
        // TODO: not very efficient, but enough for now; replace it with something faster in the future
        return UUID.randomUUID().toString();
    }

    static <O> O instantiateSafely(Class<O> clazz) {
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
    
}
