package pl.edu.agh.ap.exception;

public class CannotLoadConfigurationException extends RuntimeException {

    public CannotLoadConfigurationException(String message) {
        super(message);
    }
}
