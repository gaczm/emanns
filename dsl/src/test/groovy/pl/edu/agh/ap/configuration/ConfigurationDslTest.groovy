package pl.edu.agh.ap.configuration

import org.junit.Test
import pl.edu.agh.ap.agent.AgentBehavior

import static org.assertj.core.api.Assertions.assertThat
import static ConfigurationDsl.configuration

class ConfigurationDslTest {
    
    static class Foo extends AgentBehavior {
        @Override void doStep(int stepNumber) { }
    }

    static class Bar extends AgentBehavior {
        @Override void doStep(int stepNumber) { }
    }
    
    void foo(Runnable x) {
        x.run()
    }
    
    @Test
    public void should_load_agents_hierarchy_properly() {
        Configuration configuration = configuration {
            workplace {
                agent {
                    name = "A"
                    agentClass = Foo

                    agent {
                        name = "A.1"
                        agentClass = Foo
                        agent {
                            name = "A.1.1"
                            agentClass = Bar
                        }
                    }
                    agent {
                        name = "A.2"
                        agentClass = Bar
                    }
                }
                agent {
                    name = "B"
                    agentClass = Bar
                }
                agent {
                    name = "C"
                    agentClass = Foo
                }
            }
        }
        
        final hierarchy = configuration.workplaces.get(0).agents
        
        assertThat(hierarchy).hasSize(3) // A, B, C
        assertThat(hierarchy.get(0).name).isEqualTo("A")
        assertThat(hierarchy.get(0).agentClass).isEqualTo(Foo)
        assertThat(hierarchy.get(0).children).hasSize(2) // A.1, A.2

        assertThat(hierarchy.get(0).children.get(0).name).isEqualTo("A.1")
        assertThat(hierarchy.get(0).children.get(0).agentClass).isEqualTo(Foo)
        assertThat(hierarchy.get(0).children.get(0).children).hasSize(1) // A.1.1
        
        assertThat(hierarchy.get(0).children.get(0).children.get(0).name).isEqualTo("A.1.1")
        assertThat(hierarchy.get(0).children.get(0).children.get(0).agentClass).isEqualTo(Bar)
        assertThat(hierarchy.get(0).children.get(0).children.get(0).children).isEmpty()

        assertThat(hierarchy.get(0).children.get(1).name).isEqualTo("A.2")
        assertThat(hierarchy.get(0).children.get(1).agentClass).isEqualTo(Bar)
        assertThat(hierarchy.get(0).children.get(1).children).isEmpty()

        assertThat(hierarchy.get(1).name).isEqualTo("B")
        assertThat(hierarchy.get(1).agentClass).isEqualTo(Bar)
        assertThat(hierarchy.get(1).children).isEmpty()
        
        assertThat(hierarchy.get(2).name).isEqualTo("C")
        assertThat(hierarchy.get(2).agentClass).isEqualTo(Foo)
        assertThat(hierarchy.get(2).children).isEmpty()
    }
    
}
