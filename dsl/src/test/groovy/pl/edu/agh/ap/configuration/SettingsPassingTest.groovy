package pl.edu.agh.ap.configuration

import org.junit.Test
import pl.edu.agh.ap.Platform
import pl.edu.agh.ap.agent.AgentBehavior

import static org.assertj.core.api.Assertions.assertThat

class SettingsPassingTest {
    
    static final KEY1 = "foo"
    static final VALUE1 = 1
    static final VALUE2 = "some property"
    static final KEY2 = "bar"

    static Map<String, Object> actualSettings

    static class A extends AgentBehavior {

        @Override
        void doStep(int stepNumber) {
            SettingsPassingTest.actualSettings = getSettings()
        }
    }
    
    @Test
    void should_pass_settings_properly() {
        actualSettings = null
        
        final configuration = ConfigurationLoader.load("""
            final SETTINGS = [
                "$KEY1": $VALUE1,
                "$KEY2": "$VALUE2",
            ]
    
            configuration {
                computationDurationInSeconds = 1
                
                workplace {
                    agent {
                        agentClass = pl.edu.agh.ap.configuration.SettingsPassingTest.A
                        settings = SETTINGS
                    }
                }
            }
             """)

        final platform = new Platform(configuration)
        platform.run()
        
        assertThat((Map)actualSettings)
                .hasSize(2)
                .containsEntry(KEY1, VALUE1)
                .containsEntry(KEY2, VALUE2)
    }
    
}
