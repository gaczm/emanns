package pl.edu.agh.ap.configuration;

import org.junit.Test;
import pl.edu.agh.ap.agent.AgentBehavior;

public class ConfigurationLoaderTest {

    public static class Foo extends AgentBehavior {

        @Override
        public void doStep(int step) {
        }
    }

    @Test
    public void foo() {
        Configuration config = ConfigurationLoader.load(this.getClass().getClassLoader().getResourceAsStream("sample.cfg"));
    }
}