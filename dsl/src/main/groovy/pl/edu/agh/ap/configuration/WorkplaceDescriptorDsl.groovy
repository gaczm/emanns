package pl.edu.agh.ap.configuration

import pl.edu.agh.ap.action.Action

class WorkplaceDescriptorDsl implements WorkplaceDescriptor {
    
    private List<AgentDescriptor> agents = []
    List<Class<Action>> actions = []

    def agent(@DelegatesTo(AgentDescriptorDsl) Closure<Object> closure) {
        def child = new AgentDescriptorDsl(null)
        agents.add(child)

        closure.resolveStrategy = Closure.DELEGATE_FIRST
        closure.delegate = child
        closure()
    }

    @Override
    List<AgentDescriptor> getAgents() {
        return agents
    }
}
