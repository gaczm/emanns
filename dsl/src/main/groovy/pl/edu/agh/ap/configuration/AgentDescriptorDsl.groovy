package pl.edu.agh.ap.configuration

import groovy.transform.EqualsAndHashCode
import pl.edu.agh.ap.action.Action
import pl.edu.agh.ap.agent.AgentBehavior
import pl.edu.agh.ap.exception.CannotLoadConfigurationException

@EqualsAndHashCode
class AgentDescriptorDsl implements AgentDescriptor {
    
    private final AgentDescriptorDsl parent
    private final List<AgentDescriptorDsl> children = []
    private Map<String, Object> settings = [:]

    Class<? extends AgentBehavior> agentClass
    String name = null
    List<Class<Action>> actions = []
    int quantity = 1

    AgentDescriptorDsl(AgentDescriptorDsl parent) {
        this.parent = parent
    }

    def agent(@DelegatesTo(AgentDescriptorDsl) Closure<Object> closure) {
        def child = new AgentDescriptorDsl(this)
        children.add(child)

        closure.resolveStrategy = Closure.DELEGATE_FIRST
        closure.delegate = child
        closure()
    }

    def propertyMissing(String name) {
        throw new CannotLoadConfigurationException("Unknown property: $name, at agent: $agentPath")
    }

    def propertyMissing(String name, value) {
        throw new CannotLoadConfigurationException("Unknown property: $name, at agent: $agentPath")
    }

    private String getAgentPath() {
        (parent != null ? parent.agentPath + " >> " : "") + (name != null ? name : "<anonymous agent>")
    }

    private void printHierarchy() {
        printHierarchy(this, 0)
    }

    private void printHierarchy(AgentDescriptorDsl agent, int indent) {
        indent.times { print '  ' }
        println agent
        agent.children.each { child ->
            printHierarchy(child, indent + 1)
        }
    }


    @Override
    public String toString() {
        return "Agent(" + agentClass + ")"
    }

    @Override
    List<AgentDescriptorDsl> getChildren() {
        return Collections.unmodifiableList(children)
    }

    @Override
    Optional<AgentDescriptorDsl> getParent() {
        return Optional.ofNullable(parent)
    }

    @Override
    Map<String, Object> getSettings() {
        return settings
    }

    void setSettings(Map<String, Object> settings) {
        this.settings = settings
    }
    
    void settings(Closure<Object> mapContent) {
        def builder = new MapBuilder()
        mapContent.resolveStrategy = Closure.DELEGATE_FIRST
        mapContent.delegate = builder
        mapContent()
        this.settings = builder.map
    }
    
}
