package pl.edu.agh.ap.configuration

import org.slf4j.Logger
import org.slf4j.LoggerFactory

class InfiniteStopCondition implements StopCondition {

    private static final Logger logger = LoggerFactory.getLogger(InfiniteStopCondition)

    @Override
    boolean isReached() {
        logger.warn("Using infinte stop condition!")
        return false
    }

}
