package pl.edu.agh.ap.configuration

class MapBuilder {
    
    def map = [:]

    def propertyMissing(String name, value) {
        map[name] = value
    }

    def propertyMissing(String name) {
        map[name]
    }

    def getMap() {
        return Collections.unmodifiableMap(map)
    }
    
}
