package pl.edu.agh.ap.configuration

import pl.edu.agh.ap.exception.CannotLoadConfigurationException

import java.time.Duration

/**
 * Entry point to configuration DSL. Reading about Groovy DSL support is recommended
 */
class ConfigurationDsl implements Configuration {
    
    private List<WorkplaceDescriptorDsl> workplaces = []
    private StopCondition stopCondition = new InfiniteStopCondition()
    
    static Configuration configuration(@DelegatesTo(ConfigurationDsl) Closure<Object> closure) {
        final config = new ConfigurationDsl()
        closure.delegate = config
        closure.resolveStrategy = Closure.DELEGATE_FIRST
        closure()
        return config
    }
    
    void workplace(@DelegatesTo(WorkplaceDescriptorDsl) Closure<Object> closure) {
        final config = new WorkplaceDescriptorDsl()
        closure.delegate = config
        closure.resolveStrategy = Closure.DELEGATE_FIRST
        closure()
        workplaces.add(config)
    }

    def propertyMissing(String name) {
        throw new CannotLoadConfigurationException("Unknown property: $name")
    }

    def propertyMissing(String name, value) {
        throw new CannotLoadConfigurationException("Unknown property: $name")
    }

    @Override
    List<WorkplaceDescriptor> getWorkplaces() {
        return workplaces
    }

    @Override
    StopCondition getStopCondition() {
        return stopCondition
    }
    
    void setStopCondition(Class<? extends StopCondition> stopConditionClass) {
        stopCondition = stopConditionClass.newInstance()
    }

    void setStopCondition(StopCondition stopCondition) {
        this.stopCondition = stopCondition
    }

    void setComputationDurationInSeconds(long seconds) {
        this.stopCondition = new TimedStopCondition(Duration.ofSeconds(seconds))
    }
    
    void repeat(int times, Closure block) {
        block.delegate = this
        block.resolveStrategy = Closure.DELEGATE_FIRST
        
        for (int i = 0; i < times; ++i) {
            block(i)
        }
    }

}
