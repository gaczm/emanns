package pl.edu.agh.ap.configuration;

import groovy.lang.GroovyShell;
import org.codehaus.groovy.control.CompilerConfiguration;
import org.codehaus.groovy.control.customizers.ImportCustomizer;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;

import static pl.edu.agh.ap.misc.TimeMeasurement.measureTime;

public class ConfigurationLoader {

    public static Configuration load(Reader configuration) {
        return measureTime(() -> {
            ImportCustomizer importCustomizer = new ImportCustomizer();
            importCustomizer.addStaticImport("pl.edu.agh.ap.configuration.ConfigurationDsl", "configuration");

            CompilerConfiguration compilerConfiguration = new CompilerConfiguration();
            compilerConfiguration.addCompilationCustomizers(importCustomizer);

            GroovyShell gs = new GroovyShell(compilerConfiguration);

            return (Configuration) gs.evaluate(configuration);
        }, "Configuration loaded in: ");
    }
    
    public static Configuration loadFromClassPath(String configuration) {
        return load(ConfigurationLoader.class.getClassLoader().getResourceAsStream(configuration));
    }

    public static Configuration load(InputStream configuration) {
        return load(new InputStreamReader(configuration));
    }

    public static Configuration load(String configuration) {
        return load(new StringReader(configuration));
    }
}
